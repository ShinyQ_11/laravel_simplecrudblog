<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

      //Apabila secara default namanya akan menjadi id
    protected $primaryKey = 'id_post'; // Apabila Ingin Menggunakan id Dengan Nama Sendiri

    //Apabila secara default nama tabel akan menjadi posts
    protected $table = "post"; // Apabila Ingin Menggunakan DB Tabel Dengan Nama Sendiri

    protected $fillable = ['judul','deskripsi']; // Berfungsi untuk membuka data kolom yang dapat diisi ke DB
    protected $guarded = ['created_at','updated_at']; // Blacklist yang tidak boleh diimput otomatis
    
    
}
