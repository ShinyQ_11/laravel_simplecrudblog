<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;

class BlogController extends Controller
{
    public function index(){

        // insert biasa 
        // $post = new Post;
        // $post->judul = 'jakarta';
        // $post->deskripsi = 'jakartajakartajakartajakartajakartajakarta';
        // $post->save();


        // Insert Mass Assigner
        // Post::create([
        //     'judul'=>'Bekasi',
        //     'deskripsi'=>'BekasiBekasiBekasiBekasiBekasiBekasiBekasi'
        // ]);

        // Test Update First Satu Item 
        // $post = Post::where('judul','jakarta')->first();
        // dd($post);   
        
        // Update First Satu Item 
        // $post = Post::where('judul','jakarta')->first();
        // $post->judul = 'halo jakarta';
        // $post->save();     

        // Update Mass Assignment
        // Post::find(5)->update([
        //     'judul' => 'jakarta',
        //     'deskripsi' => 'Ini Jakarta Bos'
        // ]);
        
        // Soft Deletes
        // $post = Post::find(2);
        // $post->delete();
        
        // Menampilkan Dari Soft deletes
        // $post = Post::withTrashed()->get();
        
        // Mengembalikan Soft Deletes Dengan ID
        // Post::withTrashed()->find(2)->restore();

        
        $post = Post::all();
        return view('blog/home',['post'=> $post]);
    }

    public function detail($id){

        // $nilai = 'ini Adalah Blog Ke-' . $id;
        // $user = 'Shinyq';
        // $unescaped ='<script> alert("x!")</script>';

        // DB::table('users')->insert([
        //     'username' => 'rere',
        //     'password' => '2345'
        // ]);

        // DB::table('users')->where('username','adrian')->update([
        //     'password' => '21345'
        // ]);

        // DB::table('users')->where('username','rere')->delete();

        // $users = DB::table('users')->get();

        // $users = DB::table('users')->where('username', 'like', '%i%')
        //                            ->get();

        
        //dd($users);

        // return view('blog/single', [
        //     'blog' => $nilai, 
        //     'user'=> $user, 
        //     'users' => $users, 
        //     'unescaped' => $unescaped
        // ]);
    

        $post = Post::find($id);

        if(!$post)
            abort('404');
            
        return view('blog/single', ['post'=> $post]);

    }

    
    public function tambah(){
        return view('blog/tambah');
    }

    public function store(Request $request){

        $this->validate($request, [
            'judul'        => 'required|min:5',
            'deskripsi'    => 'required|min:5'
        ]);

        $post = new Post;
        $post->judul = $request->judul;
        $post->deskripsi = $request->deskripsi;
        $post->save();     

        return redirect('blog');
    }

    
    public function hapus($id){
        $post = Post::find($id);
        $post->delete();
        return redirect('blog');
    }

    public function edit($id){

        
        $post = Post::find($id);

        if(!$post)
            abort('404');
            
        return view('blog/edit', ['post'=> $post]);
    }


    public function update(Request $request, $id){

        $this->validate($request, [
            'judul'        => 'required|min:5',
            'deskripsi'    => 'required'
        ]);

        $post = Post::find($id);
        $post->judul = $request->judul;
        $post->deskripsi = $request->deskripsi;
        $post->save();     

        if(!$post)
            abort('404');
            
        return redirect('blog/'. $id);
    }

    

}