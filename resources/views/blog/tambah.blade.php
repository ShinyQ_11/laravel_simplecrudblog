@extends('layout.master')

@section('title', 'Tambah Post')

@section('content')

    {{-- @if(count($errors) > 0)
        <ul>
         @foreach ($errors->all() as $error)
        <li> {{ $error }}</li>
         @endforeach  
        </ul> 
    @endif --}}

<h2>Tambah Data Post</h2>

<form action="/blog" method="post">
Judul: <input type="text" name="judul" value="{{ old('judul') }}"> <br>
   
    @if($errors->has('judul'))
    <P> {{ $errors->first('judul')}}</P>
    @endif
<br>

Deskripsi : <br>
<textarea name="deskripsi" rows="8" cols="40">{{ old('deskripsi') }}</textarea>

@if($errors->has('deskripsi'))
<P> {{ $errors->first('deskripsi')}}</P>
@endif

<br>

<input type="submit" name="submit" value="Tambah Post">

{{ csrf_field() }}



</form>

@endsection
