@extends('layout.master')

@section('title', 'Edit Data Post')

@section('content')

    {{-- @if(count($errors) > 0)
        <ul>
         @foreach ($errors->all() as $error)
        <li> {{ $error }}</li>
         @endforeach  
        </ul> 
    @endif --}}

<h2>Edit Data Blog</h2>

<form action="/blog/{{$post->id_post}}" method="post">
Judul: <input type="text" name="judul" value="{{ $post->judul }}"> <br><br>

    @if($errors->has('judul'))
        <P> {{ $errors->first('judul')}}</P>
    @endif

Deskripsi : <br>
<textarea name="deskripsi" rows="8" cols="40">{{ $post->deskripsi}}</textarea><br>
<input type="submit" name="submit" value="Edit Post">

    @if($errors->has('deskripsi'))
        <P> {{ $errors->first('deskripsi')}}</P>
    @endif

{{ csrf_field() }}
 <input type="hidden" name="_method" value="PUT">



</form>

@endsection
