<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> @yield('title')</title>
</head>
<body>
    <header>
        <nav>
            <a href="/">Home</a>
            <a href="/">Blog</a>
            <a href="/blog/tambah">+ Tambah Post</a>
        </nav>
    </header>
    
    <br>
    @yield('content')
    <br>

    <footer>
        <p>
            &copy; laravel @ Blog SAYA ! 
        </p>
    </footer>

</body>
</html>