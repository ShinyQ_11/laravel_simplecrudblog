<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index');
Route::get('/blog', 'BlogController@index');

Route::get('/blog/tambah', 'BlogController@tambah');
Route::post('/blog', 'BlogController@store');

Route::get('/blog/{id}', 'BlogController@detail');

Route::get('/blog/{id}/edit/', 'BlogController@edit');
Route::put('/blog/{id}', 'BlogController@update');

Route::get('/blog/hapus/{id}', 'BlogController@hapus');

